<?php

namespace App\Http\Controllers;

use App\Period;
use App\Assignment;
use Illuminate\Http\Request;

class PeriodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $periods = Period::all();

        return view('period.index')->withPeriods($periods);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Period  $period
     * @return \Illuminate\Http\Response
     */
    public function show(Period $period){
        //$class = Period::where('id',$period)->get();
        $assignments = Period::with('assignments')->where('id',$period->id)->get();
//        return view('period.show')->with('period', $period)
//            ->with('assignments', $assignments);
        return redirect()->route('period.assignments.index', $period->id);
    }

}
