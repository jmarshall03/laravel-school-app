<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Period;

class Assignment extends Model{
    public function periodAssignments(){
        return $this->belongsTo(Period::class);
    }
}
