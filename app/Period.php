<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Assignment;

class Period extends Model{

    public function assignments(){
        return $this->hasMany(Assignment::class);
        //return $this->hasMany('App\Assignment');
        //return $this->hasMany(Assignemnt::class,'period_id', 'id');
    }
    
}
