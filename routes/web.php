<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'PeriodsController@index');
Route::get('/period/{period}', 'PeriodsController@show')->name('periodShow');
Route::resource('period.assignments', 'AssignmentController')->except('destroy','update');
//Route::resource('period', 'PeriodsController')->except('create', 'update', 'edit', 'destroy', 'store');