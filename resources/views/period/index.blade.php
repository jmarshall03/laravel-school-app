@extends('layouts.main')
@section('content')
    <ul id="showAll">
        @forelse($periods as $period)
            <li class="classPeriod">
                <h5>
                    <a href="period/{{$period->id}}">{{$period->period_name}}</a></h5>
            </li>
        @empty
            No Periods to Show
        @endforelse
    </ul>
@stop 